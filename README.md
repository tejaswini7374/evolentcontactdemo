# Project Title - EvolentContactTask

Hello,
My name is Tejaswini Patil (email-id : tejaswinipatil7575@gmail.com) As per our descussion through mail,
I am completed the given task of the api for contact info. 

## Getting Started

This task is successfully run on local machine, and also tested using POSTMAN, after getting clone of repository you will find the 
EvolentDb.Script file in the folder please execute it in Sql Server Management Studio on your machine. 
and also please make changes in web.config  > connectionString, you will find connectionStrings in
1)Contact_Info_API > Web.Config
2)Contact_Info_DAL > App.Config

```

```

```
<connectionStrings><add name="ContactInformationContext" connectionString="data source=.\SQLEXPRESS;initial catalog=EvolentDb;user id=sa;password=123;multipleactiveresultsets=True;application name=EntityFramework" providerName="System.Data.SqlClient" /></connectionStrings>

```
### Prerequisites

What things you need to install the software and how to install them!!!!
 
```
Visual Studio 2015
.NETFramework,Version=v4.6'
entity framework 6.1.3
unity web api 5.4.0 > For Dependency injection
SQL Server Management Studio 2014
PostMan > Used to test the APIs
```

### APIs List with Request and Response


```

API - getContactInfoById  
Description : Get all contact list by id 
URL: api/Contact/getContactInfoById/id
Ex-http://localhost:52069/api/Contact/getContactInfoById/2
HTTP Verb: GET
Response→
{
    "data": {
        "ContactId": 2,
        "FirstName": "Sonali",
        "LastName": "PATIL",
        "Email": "sonali12@gmail.com",
        "PhoneNumber": "9878777777",
        "Status": "ACTIVE"
    },
    "status_code": 200,
    "status_message": "OK",
    "message": "Details fetched successfully"
}


2.
API - GetContactInfoList
Description : Get all contact list
URL: api/Contact/getContactInfoList
Ex-http://localhost:52069/api/Contact/getContactInfoList
HTTP Verb: GET
Response→
{
    "data": [
        {
            "ContactId": 1,
            "FirstName": "Tejaswini",
            "LastName": "PATIL",
            "Email": "tejaswinipatil7575@gmail.com",
            "PhoneNumber": "9878777777",
            "Status": "INACTIVE"
        },
        {
            "ContactId": 2,
            "FirstName": "Meenal",
            "LastName": "SHARMA",
            "Email": "meenals@gmail.com",
            "PhoneNumber": "9867876855",
            "Status": "ACTIVE"
        }
    ],
    "status_code": 200,
    "status_message": "OK",
    "message": "Details fetched successfully",
  
}

3.
API - saveContact
Description : To save contact details
URL: api/Contact/saveContact
Ex-http://localhost:52069/api/Contact/saveContact
HTTP Verb: POST
REQUEST->
{
	
     "FirstName":"Adishree",
      "LastName":"Mehta",
      "Email":"adi@gmail.com",
      "PhoneNumber":"98577573",
      "Status":"ACTIVE"
}
Response→
{
    "data": 3,
    "status_code": 200,
    "status_message": "OK",
    "message": "A new resource was successfully created.",
  
}
4.
API - editContact
Description : To Update contact details
URL: api/Contact/editContact
Ex-http://localhost:52069/api/Contact/editContact
HTTP Verb: PUT
REQUEST->
{
	"ContactId":2,
     "FirstName":"Adishree",
      "LastName":"Mehta",
      "Email":"adi@gmail.com",
      "PhoneNumber":"98577573",
      "Status":"INACTIVE"
}
Response→
{
    "data": 2,
    "status_code": 200,
    "status_message": "OK",
    "message": "A new resource was successfully updated.",
  
}


```

## Versioning

```
v1.0.0.0
```
## Authors

* **Tejaswini Patil** - *Evolent Task*
