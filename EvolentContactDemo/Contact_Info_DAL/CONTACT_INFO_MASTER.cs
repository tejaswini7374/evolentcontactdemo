namespace Contact_Info_DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CONTACT_INFO_MASTER
    {
        [Key]
        public long CIM_ID { get; set; }

        [Required]
        [StringLength(100)]
        public string CIM_FIRSTNAME { get; set; }

        [Required]
        [StringLength(150)]
        public string PTM_LASTNAME { get; set; }

        [Required]
        [StringLength(50)]
        public string PTM_EMAIL { get; set; }

        [Required]
        [StringLength(50)]
        public string PTM_PHONENUMBER { get; set; }

        [Required]
        [StringLength(1)]
        public string PTM_STATUS { get; set; }

        public long? PTM_ADDUSER { get; set; }

        public DateTime? PTM_ADDDATE { get; set; }

        public long? PTM_MODUSER { get; set; }

        public DateTime? PTM_MODDATE { get; set; }
    }
}
