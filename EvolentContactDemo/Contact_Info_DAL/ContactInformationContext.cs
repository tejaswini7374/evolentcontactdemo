namespace Contact_Info_DAL
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ContactInformationContext : DbContext
    {
        public ContactInformationContext()
            : base("name=ContactInformationContext")
        {
        }

        public virtual DbSet<CONTACT_INFO_MASTER> CONTACT_INFO_MASTER { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CONTACT_INFO_MASTER>()
                .Property(e => e.CIM_FIRSTNAME)
                .IsUnicode(false);

            modelBuilder.Entity<CONTACT_INFO_MASTER>()
                .Property(e => e.PTM_LASTNAME)
                .IsUnicode(false);

            modelBuilder.Entity<CONTACT_INFO_MASTER>()
                .Property(e => e.PTM_EMAIL)
                .IsUnicode(false);

            modelBuilder.Entity<CONTACT_INFO_MASTER>()
                .Property(e => e.PTM_PHONENUMBER)
                .IsUnicode(false);

            modelBuilder.Entity<CONTACT_INFO_MASTER>()
                .Property(e => e.PTM_STATUS)
                .IsUnicode(false);
        }
    }
}
