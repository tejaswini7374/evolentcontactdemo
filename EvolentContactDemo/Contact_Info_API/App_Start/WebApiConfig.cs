﻿using Contact_Info_API.App_Start;
using Contact_Info_API.ExLogger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;

namespace Contact_Info_API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            // Add Custom validation filters  
            config.Filters.Add(new ValidateModelStateFilter());
            //Register Exception Handler  
            config.Services.Add(typeof(IExceptionLogger), new ExceptionManagerApi());
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new {id = RouteParameter.Optional }
            );
           
        }
    }
}
