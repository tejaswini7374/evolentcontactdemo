﻿using Contact_Info_BAL.Repositories.Repo_Implementation;
using Contact_Info_BAL.Repositories.Repo_Interface;
using Contact_Info_DAL;
using System.Configuration;
using System.Web.Http;
using Unity;
using Unity.Injection;
using Unity.WebApi;

namespace Contact_Info_API
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers
            var connectionString = ConfigurationManager.ConnectionStrings["ContactInformationContext"].ConnectionString;//Added By Tejaswini on 15/10/2019
            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterType<IContactRepository, ContactRepository>();//Added By Tejaswini on 15/10/2019
            //container.RegisterType<ContactInfoContext, ContactRepository>();
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
            new InjectionConstructor(connectionString);
        }
    }
}