﻿using Contact_Info_BAL.CommonClass;
using Contact_Info_BAL.Repositories.Repo_Interface;
using Contact_Info_BAL.ViewModel;
using Contact_Info_DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Contact_Info_API.Controllers
{
    public class ContactController : ApiController
    {

        private IContactRepository _icontactinfo;
        private ContactInformationContext _context;


        public ContactController(ContactInformationContext context, IContactRepository icontactinfo)
        {
            _icontactinfo = icontactinfo;
            _context = context;
        }

      
        /// <summary>
        /// Developer:Tejaswini Patil
        /// Date:14-10-2019
        /// </summary>
        /// <param name="ContactData"></param>
        /// <returns></returns>
        // POST: api/Contact/saveContact
        [HttpPost]
        [Route("api/Contact/saveContact")]
        public async Task<ResponseViewModel> saveContact([FromBody] ContactViewModel ContactData)
        {
            try
            {

                if (!ModelState.IsValid)
                {
                    return new ResponseViewModel(null, null, ApiResCode.BadRequest, ApiRespMessage.ParametersMissing);

                }
                long contactId = await _icontactinfo.saveContactInfo(ContactData);
                if (contactId==0)
                {
                    return new ResponseViewModel("id", contactId, ApiResCode.BadRequest, ApiRespMessage.ParametersMissing);
                }
                else
                {
                    return new ResponseViewModel("id", contactId, ApiResCode.OK, ApiRespMessage.Created);

                }


            }
            catch (Exception ex)
            {
                return new ResponseViewModel(null, null, ApiResCode.BadRequest,ex.ToString());
            }
        }
        /// <summary>
        /// Developer:Tejaswini Patil
        /// Date:14-10-2019
        /// </summary>
        /// <param name="ContactData"></param>
        /// <returns></returns>
        //PUT:api/Contact/editContact
        [HttpPut]
        [Route("api/Contact/editContact")]
        public async Task<ResponseViewModel> editContact([FromBody] ContactViewModel ContactData)
        {
            try
            {

                if (!ModelState.IsValid)
                {
                    return new ResponseViewModel(null, null, ApiResCode.BadRequest, ApiRespMessage.ParametersMissing);

                }
                long contactId = await _icontactinfo.editContactInfo(ContactData);
                if (contactId == 0)
                {
                    return new ResponseViewModel("Id", contactId, ApiResCode.OK, ApiRespMessage.BadRequest);
                }
                else
                {
                    return new ResponseViewModel("id", contactId, ApiResCode.OK, ApiRespMessage.Updated);
                }



            }
            catch (Exception ex)
            {
                
                return new ResponseViewModel(null, null, ApiResCode.BadRequest, ex.ToString());
            }
        }
        /// <summary>
        /// Get all details of contact Info
        /// Developer : Tejaswini Patil
        /// Date- 14/10/2019
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("api/Contact/getContactInfoList")]
        public async Task<ResponseViewModel> getContactInfoList()
        {
            try
            {
                List<ContactViewModel> contactInfoList = await _icontactinfo.getContactInfoList();
                if (contactInfoList == null || contactInfoList.Count == 0)
                {
                    return new ResponseViewModel("Contact_Data", null, ApiResCode.OK, ApiRespMessage.NoRecordsFound);
                }
                return new ResponseViewModel("Contact_Data", contactInfoList, ApiResCode.OK, ApiRespMessage.FetchDataSuccess);
            }
            catch (Exception ex)
            {
                return new ResponseViewModel(null, null, ApiResCode.BadRequest, ex.ToString());

            }
        }


        /// <summary>
        /// Get all details of contact Info
        /// Developer : Tejaswini Patil
        /// Date- 14/10/2019
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("api/Contact/getContactInfoById/{id}")]
        public async Task<ResponseViewModel> getContactInfoById(long id)
        {
            try
            {
                if (id > 0)
                {

                    var Contact_Details = await _icontactinfo.getContactInfoById(id);
                    if (Contact_Details != null)
                        return new ResponseViewModel("ContactDetails", Contact_Details, ApiResCode.OK, ApiRespMessage.FetchDataSuccess);
                    else
                        return new ResponseViewModel("ContactDetails", null, ApiResCode.OK, ApiRespMessage.NoRecordsFound);
                }
                else
                {
                    return new ResponseViewModel(null, null, ApiResCode.InternalServerError, ApiRespMessage.ParametersMissing);
                }

            }
            catch (Exception ex)
            {
                return new ResponseViewModel(null, null, ApiResCode.BadRequest, ex.ToString());

            }
        }




    }

}
