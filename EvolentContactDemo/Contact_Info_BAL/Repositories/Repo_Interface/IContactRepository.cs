﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contact_Info_BAL.ViewModel;

namespace Contact_Info_BAL.Repositories.Repo_Interface
{
   
        public interface IContactRepository
        {
            Task<long> saveContactInfo(ContactViewModel contactinfodata);
            Task<long> editContactInfo(ContactViewModel contactinfodata);
            Task<List<ContactViewModel>> getContactInfoList();
            Task<ContactViewModel> getContactInfoById(long Pat_Id);
        }
   
}
