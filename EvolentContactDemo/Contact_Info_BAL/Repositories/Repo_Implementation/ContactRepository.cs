﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Data.Entity;
using Contact_Info_BAL.Repositories.Repo_Interface;
using Contact_Info_DAL;
using Contact_Info_BAL.ViewModel;

namespace Contact_Info_BAL.Repositories.Repo_Implementation
{
    public class ContactRepository : IContactRepository
    {
       
        private ContactInformationContext _context;
        public ContactRepository(ContactInformationContext context) 
        {
            
            this._context = context;
        }
        /// <summary>
        /// Developer : Tejaswini Patil
        /// Date: 14/10/2019
        /// To Save ContactInfo into table 
        /// </summary>
        /// <param name="contactinfodata"></param>
        /// <returns></returns>
        public async Task<long> saveContactInfo(ContactViewModel contactinfodata)
        {
            try
            {
                long id = 0;
                if (contactinfodata.ContactId == 0 )//Insert Logic
                {
                    CONTACT_INFO_MASTER objcontactinfo = new CONTACT_INFO_MASTER();
                    objcontactinfo.CIM_FIRSTNAME = contactinfodata.FirstName.Trim().ToUpper();
                    objcontactinfo.PTM_LASTNAME = contactinfodata.LastName.Trim().ToUpper();
                    objcontactinfo.PTM_EMAIL = contactinfodata.Email.Trim();
                    objcontactinfo.PTM_PHONENUMBER = contactinfodata.PhoneNumber;
                    objcontactinfo.PTM_STATUS = contactinfodata.Status!=""?contactinfodata.Status.Trim().ToUpper()=="ACTIVE"?"Y":"N":"N";
                    objcontactinfo.PTM_ADDUSER = 1;
                    objcontactinfo.PTM_ADDDATE = DateTime.Now;
                    _context.CONTACT_INFO_MASTER.Add(objcontactinfo);
                    await _context.SaveChangesAsync();
                    id= objcontactinfo.CIM_ID;
               }
               
                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Developer : Tejaswini Patil
        /// Date: 14/10/2019
        /// To Update ContactInfo into table 
        /// </summary>
        /// <param name="contactinfodata"></param>
        /// <returns></returns>
        public async Task<long> editContactInfo(ContactViewModel contactinfodata)
        {
            try
            {
                long id = 0;
                var ContactData = _context.CONTACT_INFO_MASTER.Where(x => x.CIM_ID == contactinfodata.ContactId).FirstOrDefault();

                if (ContactData != null)
                {
                    //update Data
                    ContactData.CIM_FIRSTNAME = contactinfodata.FirstName.Trim().ToUpper();
                    ContactData.PTM_LASTNAME = contactinfodata.LastName.Trim().ToUpper();
                    ContactData.PTM_EMAIL = contactinfodata.Email.Trim();
                    ContactData.PTM_PHONENUMBER = contactinfodata.PhoneNumber;
                    ContactData.PTM_STATUS = contactinfodata.Status != "" ? contactinfodata.Status.Trim().ToUpper() == "ACTIVE" ? "Y" : "N" : "N";
                    await _context.SaveChangesAsync();
                    id = ContactData.CIM_ID;

                }
                return id;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Developer : Tejaswini Patil
        /// Date: 14/10/2019
        /// To Get All ContactInfo List
        /// </summary>
        /// <returns></returns>
        public async Task<List<ContactViewModel>> getContactInfoList()
        {
            try
            {
            List<ContactViewModel> data = new List<ContactViewModel>();
            data = await(from a in _context.CONTACT_INFO_MASTER
                    select new ContactViewModel
                    {
                        ContactId=a.CIM_ID,
                        FirstName = a.CIM_FIRSTNAME,
                        LastName = a.PTM_LASTNAME,
                        Email = a.PTM_EMAIL,
                        PhoneNumber = a.PTM_PHONENUMBER,
                        Status = a.PTM_STATUS=="Y"?"ACTIVE":"INACTIVE"
                    }).ToListAsync();
               
            return data;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        /// <summary>
        /// Tejaswini Patil
        /// /// Date: 14/10/2019
        /// To Get  ContactInfo by Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<ContactViewModel> getContactInfoById(long Id)
        {
            try
            {
            ContactViewModel data = await (from a in _context.CONTACT_INFO_MASTER
                    where a.CIM_ID==Id
                    select new ContactViewModel
                    {
                        ContactId=a.CIM_ID,
                        FirstName = a.CIM_FIRSTNAME,
                        LastName = a.PTM_LASTNAME,
                        Email = a.PTM_EMAIL,
                        PhoneNumber = a.PTM_PHONENUMBER,
                        Status = a.PTM_STATUS=="Y"?"ACTIVE":"INACTIVE"
                    }).FirstOrDefaultAsync();

            return data;
            }
            catch (Exception ex )
            {

                throw ex;
            }
        }

        
    }
}