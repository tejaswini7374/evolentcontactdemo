﻿using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;

namespace Contact_Info_BAL.CommonClass
{
    public class ResponseViewModel
    {
           // private string data_property_name { get; set; }
            public dynamic data { get; private set; }

            public ApiResCode status_code { get; set; }
            public string status_message { get; set; }
            public string message { get; set; }
           // public List<ResponseData> dataProperties { get; set; }


            public ResponseViewModel(string _DataPropertyName, dynamic _data, ApiResCode _status_code, string _message)
            {
                //data_property_name = _DataPropertyName;
                data = _data;

                status_code = _status_code;
                status_message = _status_code == ApiResCode.OK ? Convert.ToString(HttpStatusCode.OK) : Convert.ToString(HttpStatusCode.InternalServerError);
                message = _message;
            }

          
        
    }
    /// <summary>
    /// Developer : Tejaswini Patil
    /// Date: 14/10/2019
    /// Purpose: Response Codes for Web API
    /// </summary>
    public enum ApiResCode
    {
        OK = 200,
        Created = 201,
        BadRequest = 400,
        Unauthorized = 401,
        Forbidden = 403,
        NotFound = 404,
        MethodNotAllowed = 405,
        Conflict = 409,
        UnsupportedMediaType = 415,
        InternalServerError = 500,
        ServiceUnavailable = 503
    }
    /// <summary>
    /// Developer : Tejaswini Patil
    /// Date: 14/10/2019
    /// Purpose: Response Messages for Web API
    /// </summary>
    public static class ApiRespMessage
    {
        public readonly static string OK = "The request was successfully completed.";
        public readonly static string Created = "A new resource was successfully created.";
        public readonly static string Updated = "A new resource was successfully updated.";
        public readonly static string BadRequest = "The request was invalid.";
        public readonly static string Unauthorized = "The request did not include an authentication token or the authentication token was expired.";
        public readonly static string Forbidden = "The client did not have permission to access the requested resource.";
        public readonly static string NotFound = "The requested resource was not found.";
        public readonly static string MethodNotAllowed = "The HTTP method in the request was not supported by the resource";
        public readonly static string ServiceUnavailable = "The server was unavailable.";
        public readonly static string FetchDataSuccess = "Details fetched successfully";
        public readonly static string MissingAuthHeader = "Missing Authentication Header";
        public readonly static string NoRecordsFound = "No records found";
        public readonly static string Saved = "Saved records successfully";
        public readonly static string UserDeletedSuccess = "User Deleted Successfully";
        public readonly static string DuplicateEntry = "Duplicate entry not allowed";
        public readonly static string ParametersMissing = "Parameters Missing or Invalid";

    }

    /// <summary>
    /// Developer : Tejaswini Patil
    /// Date: 14/10/2019
    /// Http Status responses Data
    /// </summary>
    public class ResponseData
    {
        public string data_property_name { get; set; }
        public DataPropertyType data_Property_Type { get; set; }
        public long dataNumber { get; set; }
        public decimal dataDeclimal { get; set; }
        public decimal dataBool { get; set; }
        public dynamic data { get; set; }
    }
    /// <summary>
    /// Developer : Tejaswini Patil
    /// Date: 14/10/2019
    /// Http Status responses
    /// </summary>
    public enum DataPropertyType
    {
        Dynamic,
        Number,
        Decimal,
        Bool
    }
}